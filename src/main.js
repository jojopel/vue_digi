// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import UI from './ui-components'
import Builder from './builder'
import router from './router'
import settingplugin from '@/builder/Settings/utils/SettingPlugin.js'
// import uploader from 'vue-simple-uploader'
// import draggable from 'vuedraggable'
import VueSweetalert2 from 'vue-sweetalert2'
import { store } from './store/store'
// import NProgress from 'vue-nprogress'
import { sync } from 'vuex-router-sync'
import VueTouch from 'vue-touch'
import trumbowyg from 'vue-trumbowyg'
import VueClipboard from 'vue-clipboard2'
import 'trumbowyg/dist/ui/trumbowyg.css'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
// $.trumbowyg.svgPath = false

import AppSpinner from '@/components/helpers/AppSpinner'
import slideLeftTransition from '@/components/helpers/transitions/slideLeftTransition'
import UserInfo from '@/components/User/Info/Info'
import instance from '@/config/axios'
import Modal from '@/components/helpers/Modal'

Vue.use(trumbowyg)
require('./assets/scss/main.scss')
require('../node_modules/trumbowyg/dist/plugins/colors/trumbowyg.colors.min.js')
require('../node_modules/trumbowyg/dist/plugins/template/trumbowyg.template.js')
require('../node_modules/trumbowyg/dist/plugins/fontfamily/trumbowyg.fontfamily.js')
require('../node_modules/trumbowyg/dist/plugins/fontsize/trumbowyg.fontsize.js')
require('../node_modules/trumbowyg/dist/plugins/lineheight/trumbowyg.lineheight.js')
require('../node_modules/trumbowyg/dist/plugins/template/trumbowyg.template.js')
require('../node_modules/trumbowyg/dist/plugins/base64/trumbowyg.base64.js')
require('../node_modules/trumbowyg/dist/plugins/table/trumbowyg.table.js')
require('./config/auth')

Vue.component('AppSpinner', AppSpinner)
Vue.component('slideLeftTransition', slideLeftTransition)
Vue.component('UserInfo', UserInfo)
Vue.component('Modal', Modal)
Vue.prototype.$http = instance
Vue.directive('focus', {
  // When the bound element is inserted into the DOM...
  inserted: function (element) {
    element.focus()
    element.scrollIntoView({ behavior: 'smooth' })
  }
})


VueTouch.registerCustomEvent('doubletap', {
  type: 'tap',
  taps: 2
})
Vue.use(VueTouch, {name: 'v-touch'})

Vue.use(UI)
Vue.use(Builder)
Vue.use(VueSweetalert2)
Vue.use(VueClipboard)
Vue.use(settingplugin)
// Vue.use(brokenimg)
// Vue.use(draggable)
// Vue.use(NProgress)
// const nprogress = new NProgress({ parent: '.nprogress-container' })

Vue.config.devtools = true
Vue.config.productionTip = false
Vue.filter('formatSize', function (size) {
  if (size > 1024 * 1024 * 1024 * 1024) {
    return (size / 1024 / 1024 / 1024 / 1024).toFixed(2) + ' TB'
  } else if (size > 1024 * 1024 * 1024) {
    return (size / 1024 / 1024 / 1024).toFixed(2) + ' GB'
  } else if (size > 1024 * 1024) {
    return (size / 1024 / 1024).toFixed(2) + ' MB'
  } else if (size > 1024) {
    return (size / 1024).toFixed(2) + ' KB'
  }
  return size.toString() + ' B'
})

sync(store, router)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {App}
})
