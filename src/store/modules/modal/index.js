import { mutations } from './mutations'
import { actions } from './actions'

const state = {
  showModal: false
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
