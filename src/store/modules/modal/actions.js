export const actions = {
  SHOW_MODAL: (context) => {
    context.commit('SHOW_MODAL')
  },
  HIDE_MODAL: (context) => {
    context.commit('HIDE_MODAL')
  },
  TOGGLE_MODAL: (context) => {
    context.commit('TOGGLE_MODAL')
  }
}
