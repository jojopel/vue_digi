export const mutations = {
  SHOW_MODAL: (state) => {
    state.showModal = true
  },
  HIDE_MODAL: (state) => {
    state.showModal = false
  },
  TOGGLE_MODAL: (state) => {
    state.showModal = !state.showModal
  }
}
