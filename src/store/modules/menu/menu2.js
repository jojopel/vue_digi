import lazyLoading from './lazyLoading'

export default {
  path: '/components',
  meta: {
    icon: 'fa-building-o',
    expanded: false,
    label: 'Components'
  },
  // component: lazyLoading('components', true),

  children: [
    {
      name: 'Ui',
      path: '',
      component: lazyLoading('uipage', true),
      meta: {
        link: 'components/Default.vue'
      }
    },
    {
      name: 'Create User',
      path: '/create',
      component: lazyLoading('createform', true),
      meta: {
        requiresAuth: true,
        adminAuth: true,
        teacherAuth: true,
        icon: 'fa-tachometer',
        link: 'dashboard/index.vue'
      }
    }
  ]}
