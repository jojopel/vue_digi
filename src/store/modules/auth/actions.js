import axiosinstance from '@/config/axios'

export const actions = {
  LOGIN_USER: async (context, payload) => {
    try {
      const res = await axiosinstance.post('auth/signin', payload)
      context.commit('SET_USER', res.data.user)
    } catch (err) {
      console.log(err)
      context.commit('SET_USER', null)
    }
  },
  LOGOUT_USER: async (context) => {
    // const config = {headers: {'Cache-Control': 'no-cache'}}
    try {
      await axiosinstance.post('auth/logout')
      // context.commit('sidebar/HIDE_SIDEBAR', null, { root: true })
      context.commit('SET_USER', null)
    } catch (err) { console.log(err) }
  },
  GET_CURRENT_USER: async (context) => {
    try {
      const authUser = context.getters['authUser']
      if (authUser) {
        context.commit('SET_AUTH_STATE', true)
        return authUser
      } else {
        const res = await axiosinstance.get('auth/me')
        context.commit('SET_USER', res.data.user)
        context.commit('SET_AUTH_STATE', true)
        return res.data.user
      }
    } catch (err) {
      console.log(err)
      context.commit('SET_AUTH_STATE', true)
      // context.commit('SET_USER', null)
      return err
    }
  },
  SET_AUTH_STATE: ({commit}, payload) => {
    commit('SET_AUTH_STATE', payload)
    return true
  },
  ClEAR_USER: (context) => {
    console.log('in clear user')
    context.commit('SET_USER', null)
  }
}

