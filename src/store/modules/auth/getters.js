export const getters = {
  userIsAuthenticated: state => {
    return !!state.user
  },
  authUser: state => {
    return state.user || null
  }
}
