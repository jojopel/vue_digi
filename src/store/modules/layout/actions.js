export const actions = {
  SET_PADDING: (context, padding) => {
    context.commit('SET_PADDING', padding)
  }
}
