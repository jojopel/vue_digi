export const mutations = {
  SHOW_SIDEBAR: (state) => {
    state.isVisible = true
  },
  HIDE_SIDEBAR: (state) => {
    state.isVisible = false
  },
  TOGGLE_SIDEBAR: (state) => {
    state.isVisible = !state.isVisible
  }
}
