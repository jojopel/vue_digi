import { mutations } from './mutations'
import { actions } from './actions'

const state = {
  isVisible: true
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
