 const settingplugin = {
  install: (Vue, options) => {
    Vue.prototype.$InputNumberStep = function (tag, type, key, reversekey, step) {
      let elements = document.getElementsByTagName(tag)
      for(var i = 0; i < elements.length; i++) {
          if(elements[i].type.toLowerCase() == type) {
            elements[i].onkeyup = function (e) {
              if (e.keyCode === key && e.shiftKey) {
                e.target.value = ((parseInt(e.target.value) + parseInt(step)) - 1)
              } else if (e.keyCode === reversekey && e.shiftKey) {
                e.target.value = ((parseInt(e.target.value) - parseInt(step)) + 1)
              }
            }
          }
      }
    }
  }
}
export default settingplugin
