import MultipleChoiceImg from './MultipleChoiceImg'
import MultipleChoiceImagesGroup from './MultipleChoiceImagesGroup'

export {
  MultipleChoiceImg,
  MultipleChoiceImagesGroup
}
