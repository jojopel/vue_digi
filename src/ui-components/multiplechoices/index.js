import MultipleChoices from './MultipleChoices'
import MultipleChoicesButton from './MultipleChoicesButton.vue'
import MultipleChoicesText from './MultipleChoicesText.vue'

export { MultipleChoices, MultipleChoicesButton, MultipleChoicesText}
