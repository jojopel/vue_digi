import Field from './field'
//import Upload from './Upload'
import Input from './input'
import Icon from './icon'
import Autocomplete from './autocomplete'
import {
  Tabs,
  TabItem
} from './tabs'
import config, {
  setOptions
} from './utils/config'
import Multiselect from './select'
import {
  TapTap,
  TapTapText,
  TapTapChoice,
  TapTapInput
} from './taptap'
import TapTapChange from './taptapchange'
import draggable from 'vuedraggable'
import ChoicesTapTap from './choicestaptap'
import Grouping from './Grouping'
import {
  TrueFalse,
  TrueFalseText,
  TrueFalseObject} from './truefalse'
import {Typing, TypingInput, TypingText} from './typing'
import {MultipleChoices, MultipleChoicesButton, MultipleChoicesText} from './multiplechoices'
// import MissingLetter from './missingLetter'
// Primary
import {
  MultipleChoiceImg,
  MultipleChoiceImagesGroup
} from './multiplechoiceimg'
import GroupingImages from './GroupingImages'
import TapTapImages from './taptapimages'
import MazeTap from './mazetap'
import PrimaryTimer from './primarytimer'
import TapTapImagesOnchange from './taptapimagesonchange'
import TapTapWithImage from './taptapwithimage'
import TapTapPuzzle from './taptappuzzle'
import ChoicesImages from './choicesimages'
import CheckPrimary from './checkprimary'
// Primary
import Matchthewords from './matchthewords'
import {
  Dropdown,
  DropdownItem
} from './dropdown'
import {
  Modal2,
  ImageModal,
  CardModal
} from './modal'
import {
  Radio,
  RadioButton
} from './radio'
import {
  Checkbox,
  CheckboxButton
} from './checkbox'
import {
  VueEditor
} from './editor'
import {
  Uploader,
  UploaderBtn,
  UploaderDrop,
  UploaderUnsupport,
  UploaderList,
  UploaderFiles,
  UploaderFile
} from './uploader'
import Searchlist from './searchlist'
import fileGallery from './file-gallery'
import Pagination from './pagination'
import SvgChoices from './svgchoices'
import Svgtaps from './svgtaps'
import Svgone from './svgone'
import NavTree from './nav-tree'
import {SectionsDropDown} from './sectionsdropdown'
import {WordSearch, WordSearchLetter} from './wordsearch'
import { CrossWords, CrossWordsInput } from './crosswords'
import TapTapPuzzleMultipleRight from './taptappuzzlemultipleright'
import CheckIcons from './checkicons'
import {helpButton} from './checkprimaryhelpbuttons'
const components = {
  Field,
  Input,
  Icon,
  Tabs,
  TabItem,
  Autocomplete,
  Multiselect,
  TapTap,
  TrueFalse,
  GroupingImages,
  MultipleChoiceImg,
  TapTapImages,
  TapTapWithImage,
  TapTapImagesOnchange,
  TapTapPuzzle,
  MazeTap,
  PrimaryTimer,
  CheckPrimary,
  ChoicesImages,
  ChoicesTapTap,
  draggable,
  TapTapChange,
  Matchthewords,
  Grouping,
  Dropdown,
  DropdownItem,
  Modal2,
  ImageModal,
  CardModal,
  Radio,
  RadioButton,
  Checkbox,
  CheckboxButton,
  VueEditor,
  Uploader,
  UploaderBtn,
  UploaderDrop,
  UploaderUnsupport,
  UploaderList,
  UploaderFiles,
  UploaderFile,
  Searchlist,
  fileGallery,
  Pagination,
  SvgChoices,
  Svgtaps,
  Svgone,
  NavTree,
  SectionsDropDown,
  WordSearch,
  WordSearchLetter,
  CrossWords,
  CrossWordsInput,
  Typing,
  TypingInput,
  TypingText,
  TapTapPuzzleMultipleRight,
  NavTree,
  CheckIcons,
  TapTapText,
  TapTapChoice,
  TapTapInput,
  TrueFalseText,
  TrueFalseObject,
  MultipleChoiceImagesGroup,
  helpButton,
  MultipleChoices,
  MultipleChoicesText,
  MultipleChoicesButton
  // Topsection
}

components.install = (Vue) => {
  // setOptions(Object.assign(config, options))
  for (const componentName in components) {
    const component = components[componentName]

    if (component && componentName !== 'install') {
      Vue.component(component.name, component)
    }
  }
}
export default components
