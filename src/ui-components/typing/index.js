import Typing from './Typing'
import TypingInput from './TypingInput.vue'
import TypingText from './TypingText.vue'

export { Typing, TypingInput, TypingText }
