export default {
  props: {
    choices: {
      type: Array,
      required: true
    }
  }
}
