import VQuill from 'quill'
const Quill = window.Quill || VQuill
const Parchment = Quill.import('parchment')
const Size = Quill.import('attributors/style/size')

class CustomStyleInline extends Parchment.Attributor.Style {
  add (node, value) {
    let inlineStyle = ''
    try {
      value = JSON.parse(value)
      Object.keys(value).map((key) => {
        if (value[key] !== null) {
          value[key] = parseInt(value[key])
          inlineStyle += ` ${value[key]}px `
        } else {
          inlineStyle += ` ${0}px `
        }
      })
      if (inlineStyle === '' || inlineStyle === '0px') {
        this.remove(node)
        return true
      } else {
        return super.add(node, inlineStyle)
      }
    } catch (error) {
      return super.add(node, value)
    }
  }
}

const PaddingStyle = new CustomStyleInline('padding', 'padding', {
  scope: Parchment.Scope.INLINE
})
Size.whitelist = ['12px', '14px', '16px', '18px', '20px', '22px', '24px', '26px', '28px', '30px', '32px', '34px', '36px']

export {PaddingStyle, Size}
