import has from 'lodash/has'
import toPath from 'lodash/toPath'
export default{
  methods: {
    /**
     * [defaultDynamicObject create deep object attribute if not exists]
     * @param  {[type]} object       [localstorage object]
     * @param  {[type]} path         [path (string) to attribute without object prefix]
     * @param  {[type]} defaultValue [value]
     * @return {[type]}              [description]
     */
    defaultDynamicObjectPath (object, path, defaultValue) {
      if(!has(object, path)) {
        let tempCheckObject = object
        toPath(path).map((value) => {
          if(!has(tempCheckObject, value)) {
            this.$set(tempCheckObject, path, defaultValue)
          } else {
            tempCheckObject = tempCheckObject[value]
          }
        })
      }
    },
    /**
     * [defaultDynamicObject create deep object attribute if not exists]
     * @param  {[type]} object       [localstorage object]
     * @param  {[type]} path         [path (string) to attribute without object prefix]
     * @param  {[type]} defaultValue [value]
     * @return {[type]}              [description]
     */
    defaultDynamicObject (object, path, defaultValue) {
      if (!has(object, path)) {
        let tempCheckObject = object
        toPath(path).map((value) => {
          if (!has(tempCheckObject, value)) {
            this.$set(tempCheckObject, defaultValue)
          } else {
            tempCheckObject = tempCheckObject[value]
          }
        })
      }
    }
  }
}
