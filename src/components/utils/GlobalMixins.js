export default{
  methods: {
    /**
     * Convert a date string to current local date format
     * @param  {[type]} date [description]
     * @return {[type]}      [description]
     */
    todateformat (date) {
      let d = new Date(date)
      return d.toLocaleString()
    },
    /**
     * Return only date, not time in local pc format
     * @param  {[type]} date [description]
     * @return {[type]}      [description]
     */
    todateformat_date(date) {
      const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"]
      let d = new Date(date)
      return d.toLocaleDateString('en-US', {month: 'long', day: 'numeric'}) + ', ' + d.getFullYear().toString()
    }
  }
}
