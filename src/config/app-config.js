export const APIENDPOINT = 'http://localhost:3000/'
export const MEDIAENDPOINT = 'http://localhost:3000/mediaupload/file/'
export const SOCKETIOENDPOINT = 'http://localhost:3001'
export const DIGIENDPOINT = 'https://beta.expressdigibooks.com/web/api/digipubv1/'
export const DIGITOKEN = '7c65308e0725b15a5bf6b33684efdc70a7660e4645eedcc5fb85816f2e3e24dc'
// jo
// export const LOGINSTATUS = function () {
//   const authUser = JSON.parse(window.localStorage.getItem('expUser'))
//   if (!authUser || !authUser.session) {
//     return true
//   } else {
//     return false
//   }
// }
// Server user array helpers
export const USERSTATUS_TABLE = [{ status_id: 0, status_name: 'Inactive' }, { status_id: 1, status_name: 'Active' }]
export const SECTION_AREAS = [{area_id: 1, area_name: 'HEADER'}, {area_id: 2, area_name: 'BODY'}, {area_id: 3, area_name: 'FOOTER'}]
// Components Builder Settings  Styling array helpers
export const COLORS_OBJECTS = [{
  id: 1,
  colorgroup: 'red',
  colors: [
    {colorid: 1, colorhex: '#f44336'},
    {colorid: 2, colorhex: '#ffebee'},
    {colorid: 3, colorhex: '#ffcdd2'},
    {colorid: 4, colorhex: '#ef9a9a'},
    {colorid: 6, colorhex: '#e57373'},
    {colorid: 7, colorhex: '#ef5350'},
    {colorid: 8, colorhex: '#f44336'},
    {colorid: 9, colorhex: '#e53935'},
    {colorid: 10, colorhex: '#d32f2f'},
    {colorid: 11, colorhex: '#c62828'},
    {colorid: 12, colorhex: '#b71c1c'},
    {colorid: 13, colorhex: '#ff8a80'},
    {colorid: 14, colorhex: '#ff5252'},
    {colorid: 15, colorhex: '#ff1744'},
    {colorid: 16, colorhex: '#d50000'}]
},
{
  id: 2,
  colorgroup: 'pink',
  colors: [
    {colorid: 1, colorhex: '#e91e63'},
    {colorid: 2, colorhex: '#fce4ec'},
    {colorid: 3, colorhex: '#f8bbd0'},
    {colorid: 4, colorhex: '#f48fb1'},
    {colorid: 6, colorhex: '#f06292'},
    {colorid: 7, colorhex: '#ec407a'},
    {colorid: 8, colorhex: '#e91e63'},
    {colorid: 9, colorhex: '#d81b60'},
    {colorid: 10, colorhex: '#c2185b'},
    {colorid: 11, colorhex: '#ad1457'},
    {colorid: 12, colorhex: '#880e4f'},
    {colorid: 13, colorhex: '#ff80ab'},
    {colorid: 14, colorhex: '#ff4081'},
    {colorid: 15, colorhex: '#f50057'},
    {colorid: 16, colorhex: '#c51162'}]
},
{
  id: 3,
  colorgroup: 'purple',
  colors: [
    {colorid: 1, colorhex: '#9c27b0'},
    {colorid: 2, colorhex: '#f3e5f5'},
    {colorid: 3, colorhex: '#e1bee7'},
    {colorid: 4, colorhex: '#ce93d8'},
    {colorid: 6, colorhex: '#ba68c8'},
    {colorid: 7, colorhex: '#ab47bc'},
    {colorid: 8, colorhex: '#9c27b0'},
    {colorid: 9, colorhex: '#8e24aa'},
    {colorid: 10, colorhex: '#7b1fa2'},
    {colorid: 11, colorhex: '#6a1b9a'},
    {colorid: 12, colorhex: '#4a148c'},
    {colorid: 13, colorhex: '#ea80fc'},
    {colorid: 14, colorhex: '#e040fb'},
    {colorid: 15, colorhex: '#d500f9'},
    {colorid: 16, colorhex: '#aa00ff'}]
},
{
  id: 4,
  colorgroup: 'deep purple',
  colors: [
    {colorid: 1, colorhex: '#673ab7'},
    {colorid: 2, colorhex: '#ede7f6'},
    {colorid: 3, colorhex: '#d1c4e9'},
    {colorid: 4, colorhex: '#b39ddb'},
    {colorid: 6, colorhex: '#9575cd'},
    {colorid: 7, colorhex: '#7e57c2'},
    {colorid: 8, colorhex: '#673ab7'},
    {colorid: 9, colorhex: '#5e35b1'},
    {colorid: 10, colorhex: '#512da8'},
    {colorid: 11, colorhex: '#4527a0'},
    {colorid: 12, colorhex: '#311b92'},
    {colorid: 13, colorhex: '#b388ff'},
    {colorid: 14, colorhex: '#7c4dff'},
    {colorid: 15, colorhex: '#651fff'},
    {colorid: 16, colorhex: '#6200ea'}]
},
{
  id: 5,
  colorgroup: 'indigo',
  colors: [
    {colorid: 1, colorhex: '#3f51b5'},
    {colorid: 2, colorhex: '#e8eaf6'},
    {colorid: 3, colorhex: '#c5cae9'},
    {colorid: 4, colorhex: '#9fa8da'},
    {colorid: 6, colorhex: '#7986cb'},
    {colorid: 7, colorhex: '#5c6bc0'},
    {colorid: 8, colorhex: '#3f51b5'},
    {colorid: 9, colorhex: '#3949ab'},
    {colorid: 10, colorhex: '#303f9f'},
    {colorid: 11, colorhex: '#283593'},
    {colorid: 12, colorhex: '#1a237e'},
    {colorid: 13, colorhex: '#8c9eff'},
    {colorid: 14, colorhex: '#536dfe'},
    {colorid: 15, colorhex: '#3d5afe'},
    {colorid: 16, colorhex: '#304ffe'}]
},
{
  id: 6,
  colorgroup: 'blue',
  colors: [
    {colorid: 1, colorhex: '#2196f3'},
    {colorid: 2, colorhex: '#e3f2fd'},
    {colorid: 3, colorhex: '#bbdefb'},
    {colorid: 4, colorhex: '#90caf9'},
    {colorid: 6, colorhex: '#64b5f6'},
    {colorid: 7, colorhex: '#42a5f5'},
    {colorid: 8, colorhex: '#2196f3'},
    {colorid: 9, colorhex: '#1e88e5'},
    {colorid: 10, colorhex: '#1976d2'},
    {colorid: 11, colorhex: '#1565c0'},
    {colorid: 12, colorhex: '#0d47a1'},
    {colorid: 13, colorhex: '#82b1ff'},
    {colorid: 14, colorhex: '#448aff'},
    {colorid: 15, colorhex: '#2979ff'},
    {colorid: 16, colorhex: '#2962ff'}]
},
{
  id: 7,
  colorgroup: 'Light Blue',
  colors: [
    {colorid: 1, colorhex: '#03a9f4'},
    {colorid: 2, colorhex: '#e1f5fe'},
    {colorid: 3, colorhex: '#b3e5fc'},
    {colorid: 4, colorhex: '#81d4fa'},
    {colorid: 6, colorhex: '#4fc3f7'},
    {colorid: 7, colorhex: '#29b6f6'},
    {colorid: 8, colorhex: '#03a9f4'},
    {colorid: 9, colorhex: '#039be5'},
    {colorid: 10, colorhex: '#0288d1'},
    {colorid: 11, colorhex: '#0277bd'},
    {colorid: 12, colorhex: '#01579b'},
    {colorid: 13, colorhex: '#80d8ff'},
    {colorid: 14, colorhex: '#40c4ff'},
    {colorid: 15, colorhex: '#00b0ff'},
    {colorid: 16, colorhex: '#0091ea'}]
},
{
  id: 8,
  colorgroup: 'Cyan',
  colors: [
    {colorid: 1, colorhex: '#00bcd4'},
    {colorid: 2, colorhex: '#e0f7fa'},
    {colorid: 3, colorhex: '#b2ebf2'},
    {colorid: 4, colorhex: '#80deea'},
    {colorid: 6, colorhex: '#4dd0e1'},
    {colorid: 7, colorhex: '#26c6da'},
    {colorid: 8, colorhex: '#00bcd4'},
    {colorid: 9, colorhex: '#00acc1'},
    {colorid: 10, colorhex: '#0097a7'},
    {colorid: 11, colorhex: '#00838f'},
    {colorid: 12, colorhex: '#006064'},
    {colorid: 13, colorhex: '#84ffff'},
    {colorid: 14, colorhex: '#18ffff'},
    {colorid: 15, colorhex: '#00e5ff'},
    {colorid: 16, colorhex: '#00b8d4'}]
},
{
  id: 9,
  colorgroup: 'Teal',
  colors: [
    {colorid: 1, colorhex: '#009688'},
    {colorid: 2, colorhex: '#e0f2f1'},
    {colorid: 3, colorhex: '#b2dfdb'},
    {colorid: 4, colorhex: '#80cbc4'},
    {colorid: 6, colorhex: '#4db6ac'},
    {colorid: 7, colorhex: '#26a69a'},
    {colorid: 8, colorhex: '#009688'},
    {colorid: 9, colorhex: '#00897b'},
    {colorid: 10, colorhex: '#00796b'},
    {colorid: 11, colorhex: '#00695c'},
    {colorid: 12, colorhex: '#004d40'},
    {colorid: 13, colorhex: '#a7ffeb'},
    {colorid: 14, colorhex: '#64ffda'},
    {colorid: 15, colorhex: '#1de9b6'},
    {colorid: 16, colorhex: '#00bfa5'}]
},
{
  id: 10,
  colorgroup: 'Green',
  colors: [
    {colorid: 1, colorhex: '#4caf50'},
    {colorid: 2, colorhex: '#e8f5e9'},
    {colorid: 3, colorhex: '#c8e6c9'},
    {colorid: 4, colorhex: '#a5d6a7'},
    {colorid: 6, colorhex: '#81c784'},
    {colorid: 7, colorhex: '#66bb6a'},
    {colorid: 8, colorhex: '#4caf50'},
    {colorid: 9, colorhex: '#43a047'},
    {colorid: 10, colorhex: '#388e3c'},
    {colorid: 11, colorhex: '#2e7d32'},
    {colorid: 12, colorhex: '#1b5e20'},
    {colorid: 13, colorhex: '#b9f6ca'},
    {colorid: 14, colorhex: '#69f0ae'},
    {colorid: 15, colorhex: '#00e676'},
    {colorid: 16, colorhex: '#00c853'}]
},
{
  id: 11,
  colorgroup: 'Light Green',
  colors: [
    {colorid: 1, colorhex: '#8bc34a'},
    {colorid: 2, colorhex: '#f1f8e9'},
    {colorid: 3, colorhex: '#dcedc8'},
    {colorid: 4, colorhex: '#c5e1a5'},
    {colorid: 6, colorhex: '#aed581'},
    {colorid: 7, colorhex: '#9ccc65'},
    {colorid: 8, colorhex: '#8bc34a'},
    {colorid: 9, colorhex: '#7cb342'},
    {colorid: 10, colorhex: '#689f38'},
    {colorid: 11, colorhex: '#558b2f'},
    {colorid: 12, colorhex: '#33691e'},
    {colorid: 13, colorhex: '#ccff90'},
    {colorid: 14, colorhex: '#b2ff59'},
    {colorid: 15, colorhex: '#76ff03'},
    {colorid: 16, colorhex: '#64dd17'}]
},
{
  id: 12,
  colorgroup: 'Lime',
  colors: [
    {colorid: 1, colorhex: '#cddc39'},
    {colorid: 2, colorhex: '#f9fbe7'},
    {colorid: 3, colorhex: '#f0f4c3'},
    {colorid: 4, colorhex: '#e6ee9c'},
    {colorid: 6, colorhex: '#dce775'},
    {colorid: 7, colorhex: '#d4e157'},
    {colorid: 8, colorhex: '#cddc39'},
    {colorid: 9, colorhex: '#c0ca33'},
    {colorid: 10, colorhex: '#afb42b'},
    {colorid: 11, colorhex: '#9e9d24'},
    {colorid: 12, colorhex: '#827717'},
    {colorid: 13, colorhex: '#f4ff81'},
    {colorid: 14, colorhex: '#eeff41'},
    {colorid: 15, colorhex: '#c6ff00'},
    {colorid: 16, colorhex: '#aeea00'}]
},
{
  id: 13,
  colorgroup: 'Yellow',
  colors: [
    {colorid: 1, colorhex: '#ffeb3b'},
    {colorid: 2, colorhex: '#fffde7'},
    {colorid: 3, colorhex: '#fff9c4'},
    {colorid: 4, colorhex: '#fff59d'},
    {colorid: 6, colorhex: '#fff176'},
    {colorid: 7, colorhex: '#ffee58'},
    {colorid: 8, colorhex: '#ffeb3b'},
    {colorid: 9, colorhex: '#fdd835'},
    {colorid: 10, colorhex: '#fbc02d'},
    {colorid: 11, colorhex: '#f9a825'},
    {colorid: 12, colorhex: '#f57f17'},
    {colorid: 13, colorhex: '#ffff8d'},
    {colorid: 14, colorhex: '#ffff00'},
    {colorid: 15, colorhex: '#ffea00'},
    {colorid: 16, colorhex: '#ffd600'}]
},
{
  id: 14,
  colorgroup: 'Amber',
  colors: [
    {colorid: 1, colorhex: '#ffeb3b'},
    {colorid: 2, colorhex: '#fffde7'},
    {colorid: 3, colorhex: '#fff9c4'},
    {colorid: 4, colorhex: '#fff59d'},
    {colorid: 6, colorhex: '#fff176'},
    {colorid: 7, colorhex: '#ffee58'},
    {colorid: 8, colorhex: '#ffeb3b'},
    {colorid: 9, colorhex: '#fdd835'},
    {colorid: 10, colorhex: '#fbc02d'},
    {colorid: 11, colorhex: '#f9a825'},
    {colorid: 12, colorhex: '#f57f17'},
    {colorid: 13, colorhex: '#ffff8d'},
    {colorid: 14, colorhex: '#ffff00'},
    {colorid: 15, colorhex: '#ffea00'},
    {colorid: 16, colorhex: '#ffd600'}]
},
{
  id: 15,
  colorgroup: 'Orange',
  colors: [
    {colorid: 1, colorhex: '#ff9800'},
    {colorid: 2, colorhex: '#fff3e0'},
    {colorid: 3, colorhex: '#ffe0b2'},
    {colorid: 4, colorhex: '#ffcc80'},
    {colorid: 6, colorhex: '#ffb74d'},
    {colorid: 7, colorhex: '#ffa726'},
    {colorid: 8, colorhex: '#ff9800'},
    {colorid: 9, colorhex: '#fb8c00'},
    {colorid: 10, colorhex: '#f57c00'},
    {colorid: 11, colorhex: '#ef6c00'},
    {colorid: 12, colorhex: '#e65100'},
    {colorid: 13, colorhex: '#ffd180'},
    {colorid: 14, colorhex: '#ffab40'},
    {colorid: 15, colorhex: '#ff9100'},
    {colorid: 16, colorhex: '#ff6d00'}]
},
{
  id: 16,
  colorgroup: 'Deep Orange',
  colors: [
    {colorid: 1, colorhex: '#ff5722'},
    {colorid: 2, colorhex: '#fbe9e7'},
    {colorid: 3, colorhex: '#ffccbc'},
    {colorid: 4, colorhex: '#ffab91'},
    {colorid: 6, colorhex: '#ff8a65'},
    {colorid: 7, colorhex: '#ff7043'},
    {colorid: 8, colorhex: '#ff5722'},
    {colorid: 9, colorhex: '#f4511e'},
    {colorid: 10, colorhex: '#e64a19'},
    {colorid: 11, colorhex: '#d84315'},
    {colorid: 12, colorhex: '#bf360c'},
    {colorid: 13, colorhex: '#ff9e80'},
    {colorid: 14, colorhex: '#ff6e40'},
    {colorid: 15, colorhex: '#ff3d00'},
    {colorid: 16, colorhex: '#dd2c00'}]
},
{
  id: 17,
  colorgroup: 'Brown',
  colors: [
    {colorid: 1, colorhex: '#795548'},
    {colorid: 2, colorhex: '#efebe9'},
    {colorid: 3, colorhex: '#d7ccc8'},
    {colorid: 4, colorhex: '#bcaaa4'},
    {colorid: 6, colorhex: '#a1887f'},
    {colorid: 7, colorhex: '#8d6e63'},
    {colorid: 8, colorhex: '#795548'},
    {colorid: 9, colorhex: '#6b4c41'},
    {colorid: 10, colorhex: '#5d4037'},
    {colorid: 11, colorhex: '#4e342e'},
    {colorid: 12, colorhex: '#3e2723'}]
},
{
  id: 18,
  colorgroup: 'Grey',
  colors: [
    {colorid: 1, colorhex: '#9e9e9e'},
    {colorid: 2, colorhex: '#fafafa'},
    {colorid: 3, colorhex: '#f5f5f5'},
    {colorid: 4, colorhex: '#eeeeee'},
    {colorid: 6, colorhex: '#e0e0e0'},
    {colorid: 7, colorhex: '#bdbdbd'},
    {colorid: 8, colorhex: '#9e9e9e'},
    {colorid: 9, colorhex: '#757575'},
    {colorid: 10, colorhex: '#616161'},
    {colorid: 11, colorhex: '#424242'},
    {colorid: 12, colorhex: '#212121'}]
},
{
  id: 19,
  colorgroup: 'Blue Grey',
  colors: [
    {colorid: 1, colorhex: '#607d8b'},
    {colorid: 2, colorhex: '#eceff1'},
    {colorid: 3, colorhex: '#cfd8dc'},
    {colorid: 4, colorhex: '#b0bec5'},
    {colorid: 6, colorhex: '#90a4ae'},
    {colorid: 7, colorhex: '#78909c'},
    {colorid: 8, colorhex: '#607d8b'},
    {colorid: 9, colorhex: '#546e7a'},
    {colorid: 10, colorhex: '#455a64'},
    {colorid: 11, colorhex: '#37474f'},
    {colorid: 12, colorhex: '#263238'}]
},
{
  id: 20,
  colorgroup: 'Standar',
  colors: [
    {colorid: 1, colorhex: '#ffffff'},
    {colorid: 2, colorhex: '#000000'},
    {colorid: 3, colorhex: '#fff0'}]
}
]
