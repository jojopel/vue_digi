import studyskill from '@/sections/Body/StaticPages/StudySkill.vue'
import primaryaudioplayer from '@/sections/Body/Primary/PrimaryAudioPlayer.vue'
import primaryvideoplayer from '@/sections/Body/Primary/PrimaryVideoPlayer.vue'
import textparagraph from '@/sections/Body/StaticPages/TextParagraphs.vue'
import { Modal } from 'vue-bulma-modal'

export default {
  studyskill,
  primaryaudioplayer,
  primaryvideoplayer,
  textparagraph,
  Modal
}
