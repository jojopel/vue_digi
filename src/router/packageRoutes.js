// import ManagePackages from '@/components/ManagePackages'
// import CreatePackage from '@/components/CreatePackage'
// import UpdatePackage from '@/components/UpdatePackage'
import AssignedUsers from '@/components/AssignedUsers'
// import ModuleView from '@/components/ModuleView'
import AssignedSections from '@/components/AssignedSections'
import GridPages from '@/components/GridPages'
// import Packages from '@/components/packages/Packages'
// import Package from '@/components/packages/Package'

export default [
  // {
  //   path: '/packages',
  //   name: 'Packages',
  //   component: Packages,
  //   children:
  //   [
  //   ]
  // },
  // {
  //   path: '/packages/:package_name',
  //   name: 'package',
  //   component: Package
  // },
  // {
  //   path: '/packages/grid',
  //   name: 'Packages Grid View',
  //   component: ManagePackages,
  //   meta: {
  //     requiresAuth: true,
  //     adminAuth: true,
  //     teacherAuth: true,
  //     editorAuth: true
  //   },
  //   children: [
  //   ]
  // },
  // {
  //   path: '/packages/create',
  //   name: 'Create Package',
  //   component: CreatePackage,
  //   meta: {
  //     requiresAuth: true,
  //     adminAuth: true,
  //     teacherAuth: false,
  //     testerAuth: false
  //   }
  // },
  // {
  //   path: '/packages/update',
  //   name: 'Update Package',
  //   component: UpdatePackage,
  //   meta: {
  //     requiresAuth: true,
  //     adminAuth: true,
  //     teacherAuth: false,
  //     testerAuth: false
  //   },
  //   beforeEnter: (to, from, next) => {
  //     // const { params, query } = to
  //     // if (!params) {
  //     //   next('/packages/grid')
  //     // } else {
  //     //   next()
  //     // }
  //     next()
  //   }
  // },
  {
    path: '/packages/assignedsections',
    name: 'Assign Sections',
    component: AssignedSections,
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: false,
      testerAuth: false
    }
  },
  {
    path: '/packages/assignedusers',
    name: 'Assigned Users to Packages',
    component: AssignedUsers,
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: false,
      testerAuth: false
    }
  },
  // {
  //   path: '/packages/modules',
  //   name: 'View Modules',
  //   component: ModuleView,
  //   meta: {
  //     requiresAuth: true,
  //     adminAuth: true,
  //     teacherAuth: true,
  //     editorAuth: true,
  //     testerAuth: false
  //   }
  // },
  {
    path: '/gridpages',
    name: 'Grid Pages',
    component: GridPages,
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: true,
      editorAuth: true,
      testerAuth: false
    }
  }
]
