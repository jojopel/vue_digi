import Vue from 'vue'
import Router from 'vue-router'
import MyHomePage from '@/components/MyHomePage'

import UpdateSection from '@/components/UpdateSection'

import CreatePage from '@/components/CreatePage'
// import CreateSection from '@/components/CreateSection'
import UploadPilots from '@/components/UploadPilots'
import Page403 from '@/components/Page403'
// import Page404 from '@/components/Page404'
import PageNetworkError from '@/components/PageNetworkError'
import Builder from '@/components/Builder'
// import ComponentModal from '@/ui-components/modal/ComponentModal'
// import TestRearrange from '@/components/TestRearrange'
// import { store } from '../store/store'
// import { TOGGLE_SIDEBAR } from '../store/mutation-types'
import MediaManager from '@/components/MediaManager'
import Karaoke from '@/components/Karaoke'
// import Pdf from '@/components/Pdf'
import Export from '@/components/Export'
// import Test from '@/components/Test'
import MySections from '@/components/MySections' // my sections page
import IframeView from '@/components/IframeView'

import store from '../store/store'
import PageNotFound from '@/components/helpers/PageNotFound'

import PageNotAuthorised from '@/components/helpers/PageNotAuthorised'
import PageNotAuthenticated from '@/components/helpers/PageNotAuthenticated'
import authRoutes from './authRoutes'
import bugRoutes from './bugRoutes'
import userRoutes from './userRoutes'
import packageRoutes from './packageRoutes'

import Breadcrumb from '@/components/ui/Breadcrumb'

import MyRecent from '@/components/ui/MyRecent'
import Info from '@/components/User/Info/Info'
import UpdateForm from '@/components/UpdateForm'
import CreateUser from '@/components/CreateUser'
import GridUsers from '@/components/GridUsers'

import Packages from '@/components/packages/Packages'
import Package from '@/components/packages/Package'
import ManagePackages from '@/components/ManagePackages'
import CreatePackage from '@/components/CreatePackage'
import UpdatePackage from '@/components/UpdatePackage'
import ModuleView from '@/components/ModuleView'


Vue.use(Router)
const baseRoutes = [
  {
    path: '/',
    name: 'PageHome',
    component: MyHomePage,
    children: [
      {
        path: '/user',
        name: 'User Profile',
        component: Breadcrumb,
        meta: {
          requiresAuth: true,
          adminAuth: true,
          teacherAuth: true,
          testerAuth: true
        },
        children: [
          {
            name: '/recent',
            path: 'recent',
            component: MyRecent
          },
          {
            name: '/info',
            path: 'info',
            component: Info
          },
          {
            path: '/update',
            name: 'Update User',
            component: UpdateForm
            // meta: {
            //   requiresAuth: true,
            //   adminAuth: true,
            //   teacherAuth: true,
            //   testerAuth: false
            // }
          },
          {
            path: '/user/create',
            name: 'Create New User',
            component: CreateUser
          },
          {
            path: '/usergrid',
            name: 'User Grid View',
            component: GridUsers,
            meta: {
              // requiresAuth: true,
              // adminAuth: true,
              // teacherAuth: false,
              // testerAuth: false
            }
          }
        ]
      },
      // {
      //   path: '/users/grid',
      //   name: 'User Grid View',
      //   component: GridUsers,
      //   meta: {
      //     requiresAuth: true,
      //     adminAuth: true,
      //     teacherAuth: false,
      //     testerAuth: false
      //   }
      // },
      {
        path: '/packages',
        name: 'Packages',
        component: Breadcrumb,
        children:
        [
          {
            path: '/list',
            name: 'packagelist',
            component: Packages
          },
          {
            path: '/grid',
            name: 'Packages Grid View',
            component: ManagePackages,
            meta: {
              requiresAuth: true,
              adminAuth: true,
              teacherAuth: true,
              editorAuth: true
            },
            children: [
            ]
          },
          {
            path: '/create',
            name: 'Create Package',
            component: CreatePackage,
            meta: {
              requiresAuth: true,
              adminAuth: true,
              teacherAuth: false,
              testerAuth: false
            }
          },
          {
            path: '/updatepackage',
            name: 'Update Package',
            component: UpdatePackage,
            meta: {
              requiresAuth: true,
              adminAuth: true,
              teacherAuth: false,
              testerAuth: false
            }
          },
          {
            path: '/modules',
            name: 'View Modules',
            component: ModuleView,
            meta: {
              // requiresAuth: true,
              // adminAuth: true,
              // teacherAuth: true,
              // editorAuth: true,
              // testerAuth: false
            }
          }

        ]
      },
      {
        path: '/packages/:package_name',
        name: 'package',
        component: Package
      }
      // {
      //   path: '/packages/:package_name/modules',
      //   name: 'packageModules',
      //   component: ModuleView
      // }
    ]
  },
  {
    path: '/builder',
    name: 'Builder',
    component: Builder,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/karaoke',
    name: 'Karaoke',
    component: Karaoke,
    meta: {
      requiresAuth: true
    }
  },
  // {
  //   path: '/pdf',
  //   name: 'Pdf',
  //   component: Pdf
  // },
  {
    path: '/not-allowed',
    name: 'Page403',
    component: Page403
  },
  {
    path: '/not-authenticated',
    name: 'Page401',
    component: PageNotAuthenticated
  },
  {
    path: '/networkerror',
    name: '',
    component: PageNetworkError
  },
  { path: '*',
    // component: Page404
    component: PageNotFound
  },
  {
    path: '/not-authorised',
    component: PageNotAuthorised,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/createpage',
    name: 'Create Page',
    component: CreatePage,
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: true,
      testerAuth: false
    }
  },
  {
    path: '/updatesection',
    name: 'Update Sections',
    component: UpdateSection,
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: false,
      testerAuth: false
    }
  },
  {
    path: '/uploadpilots',
    name: 'Upload Pilot',
    component: UploadPilots,
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: false,
      testerAuth: false
    }
  },
  {
    path: '/export',
    name: 'Export',
    component: Export
  },
  {
    path: '/media',
    name: 'Media',
    component: MediaManager
  },
  {
    path: '/iframeview',
    name: 'Test View',
    component: IframeView,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/mysections',
    name: 'My Sections',
    component: MySections,
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: true,
      testerAuth: false
    }
  }
]
// const scrollBehavior = (to, from, savedPosition) => {
//   if (savedPosition) {
//     return savedPosition
//   } else if (to.hash) {
//     let el = document.querySelector(to.hash)
//     return { x: 0, y: el ? el.offsetTop : 0 }
//   } else {
//     return { x: 0, y: 0 }
//   }
// }

const routes = [...baseRoutes, ...bugRoutes, ...userRoutes, ...authRoutes, ...packageRoutes]

const router = new Router({
  routes,
  base: __dirname,
  mode: 'hash',
  linkActiveClass: 'is-active',
  fallback: false
  // scrollBehavior
})

router.beforeEach(async (to, from, next) => {
  await store.dispatch('auth/GET_CURRENT_USER')

  const userIsAuthenticated = store.getters['auth/userIsAuthenticated']

  if (to.meta.requiresAuth) {
    if (userIsAuthenticated) {
      // console.log('userIsAuthenticated')
      return next()
    } else {
      return next({name: 'Page401'})
      // return next()
    }
  }
  if (to.meta.isGuest) {
    if (userIsAuthenticated) {
      console.log('isGuest/PageHome')
      return next({name: 'PageHome'})
    } else {
      console.log('isGuest/next')
      return next()
    }
  } else {
    console.log('else')
    return next()
  }

  // if (userIsAuthenticated) {
  //   console.log('userIsAuthenticated')
  //   if (to.meta.isGuest) {
  //     return next({name: 'PageHome'})
  //   } else {
  //     return next()
  //   }
  //   console.log('userIsAuthenticated')
  //   return next()
  // }

  // if (to.meta.requiresAuth) {
  //   const authUser = JSON.parse(window.localStorage.getItem('expUser'))
  //   if (!authUser || !authUser.session) {
  //     next({
  //       name: 'Login'
  //     })
  //   }
  //   if (to.meta.adminAuth || to.meta.teacherAuth || to.meta.editorAuth || to.meta.testerAuth) {
  //     if (authUser.role === 1) {
  //       next()
  //     } else if (authUser.role === 2) {
  //         next()
  //     } else if (authUser.role === 3) {
  //         next()
  //     } else if (authUser.role === 4) {
  //         next()
  //       } else {
  //         next({
  //           name: 'Page403'
  //         })
  //       }
  //   } else {
  //     next()
  //   }
  // } else {
  //   next()
  // }
})

export default router
