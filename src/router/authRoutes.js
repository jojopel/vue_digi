import LoginForm from '@/components/LoginForm'
import Signin from '@/components/Forms/Signin'

export default [
  {
    path: '/login',
    name: 'Login',
    component: LoginForm
  },
  {
    path: '/signin',
    name: 'Signin',
    component: Signin,
    meta: {
      isGuest: true
    }
  }
]
