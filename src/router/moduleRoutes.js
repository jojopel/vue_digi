import CreateModule from '@/components/CreateModule'
import UpdateModule from '@/components/UpdateModule'

export default [
  {
    path: '/modules/create',
    name: 'Create Module',
    component: CreateModule,
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: false,
      testerAuth: false
    }
  },
  { // unused?
    path: '/modules/update',
    name: 'Update Module',
    component: UpdateModule,
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: false,
      testerAuth: false
    }
  }
]
