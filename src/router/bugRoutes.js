import Bugs from '@/components/Bugs'
import EditBug from '@/components/EditBug'

export default [
  {
    path: '/bugs',
    name: 'Bugs',
    component: Bugs,
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: false,
      testerAuth: false
    }
  },
  {
    path: '/bugs/:id',
    name: 'Edit Bug',
    component: EditBug,
    meta: {
      requiresAuth: true,
      adminAuth: true,
      teacherAuth: false,
      testerAuth: false
    }
  }
]
