import Vue from 'vue'
import { mount, shallow } from 'vue-test-utils'
import grouping from '@/ui-components/Grouping/Grouping.vue'
import VueTouch from 'vue-touch'
import eventbus from '@/config/eventhub.js'

describe('Render Tap Tap correctly', () => {
  let wrapper
  const el = JSON.parse("{\"groups\":[{\"id\":1,\"name\":\"george\",\"bindchoicesid\":\"grouping\",\"rightanswer\":[]}], \"choiceid\":\"grouping\"}")
  beforeEach(() => {
    wrapper = mount(grouping, {
      propsData: { el },

    })
  })
  it('render correct html', () => {
    expect(wrapper.element).toMatchSnapshot()
  })
  it('contains v-touch', () => {
    expect(wrapper.contains('v-touch')).toBe(true)
  })
  it('check if taped group calls the usertap function', () => {
    let vtouchel = wrapper.find('v-touch')
    const spy = jest.fn()
    wrapper.setMethods({usertap: spy})
    wrapper.update()
    vtouchel.trigger('tap')
    expect(spy).toHaveBeenCalledTimes(1)
  })
  it('check event choicestaptap_userchoice', () => {
    let data = {
      text: 'userchoice',
      id: 1,
      choicestaptapid: 'grouping'
    }
    eventbus.fire('choicestaptap_userchoice', data)
    expect(wrapper.vm.disableadd).toBe(false)
  })
})
