import Vue from 'vue'
import { mount, shallow } from 'vue-test-utils'
import choicestaptap from '@/ui-components/choicestaptap/ChoicesTapTap.vue'
import VueTouch from 'vue-touch'
import eventbus from '@/config/eventhub.js'

describe('Render Choices Tap Tap correctly', () => {
  let wrapper
  const el = JSON.parse("{\"key\":\"taptapwithbg\",\"name\":\"taptap with background\",\"taps\":[{\"id\":1,\"rightanswer\":\"\",\"tapinput\":\"1)......\",\"beforetext\":\"Text before tap input \",\"aftertext\":\" Text after tap input\",\"capitalize\":false,\"choicemultiple\":false,\"bindchoicesid\":\"tap\"},{\"id\":2,\"rightanswer\":\"\",\"tapinput\":\"2)......\",\"beforetext\":\"Text before tap input \",\"aftertext\":\" Text after tap input\",\"capitalize\":false,\"choicemultiple\":false,\"bindchoicesid\":\"tap\"}],\"choices\":[{\"id\":1,\"text\":\"choice1\",\"selected\":false},{\"id\":2,\"text\":\"choice2\",\"selected\":false}],\"bgobject\":{},\"choiceid\":\"\",\"settingid\":0,\"area\":\"BODY\",\"index\":0}")
  beforeEach(() => {
    wrapper = mount(choicestaptap, {
      propsData: { el }
    })
  })
  it('render correct html', () => {
    expect(wrapper.element).toMatchSnapshot()
  })
  it('contains v-touch', () => {
    expect(wrapper.contains('v-touch')).toBe(true)
  })
  it('check if taped choice calls the choiceclicked function', () => {
    let vtoucharray = wrapper.findAll('v-touch')
    const spy = jest.fn()
    wrapper.setMethods({choiceclicked: spy})
    wrapper.update()
    for (var i = 0; i < vtoucharray.length; i++) {
      vtoucharray.at(i).trigger('tap')
    }
    expect(spy).toHaveBeenCalledTimes(vtoucharray.length)
  })
  it('check if choiceid comes from json data', () => {
    expect(wrapper.vm.choiceid).toBe(el.choiceid)
  })
})
