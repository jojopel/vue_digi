var moxios = require('moxios')
var axios = require('axios')
var fs = require('fs')
module.exports = {
  beforeEach: function (browser) {
    var object = JSON.parse(fs.readFileSync('F:/node_projects/DigiPub/vue_client/test/e2e/jsonfiles/taptapletter.json', 'utf8'))
    // import and pass your custom axios instance to this method
    moxios.install()
    moxios.stubRequest('/getjson', {
      status: 200,
      response: object
    })
  },
  'taptapletters e2e test': function (browser) {
    const builder = browser.globals.devServerURL + '/builder'
    axios.get('/getjson').then((data) => {
      browser
      .url(builder)
      .setCookie({
        name     : 'token',
        value    : 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NiwiaWF0IjoxNTE3MzA1NTc1LCJleHAiOjE1NDg4NjMxNzV9.Jx1uA-HzYmpvAHYLRvLVuBO6qnc27JsrvO6-CkgZgrQ',
        path: '/',
        domain: 'localhost'
      })
      .execute(function (data){
        window.localStorage.setItem('dropped', data)
        return true
      }, [data],  function (result) {
        console.log(data)
        this.assert.ok(result.value)
      })
      .url(builder)
    })
  },
  afterEach: function () {
    // import and pass your custom axios instance to this method
    moxios.uninstall()
  }
}
