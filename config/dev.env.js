'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  APIENDPOINT: '"http://localhost:3000/"',
  MEDIAENDPOINT: '"http://localhost:3000/mediaupload/file/"',
  SOCKETIOENDPOINT: '"http://localhost:3001"',
  DIGIENDPOINT: '"https://beta.expressdigibooks.com/web/api/digipubv1/"',
  DIGITOKEN: '"7c65308e0725b15a5bf6b33684efdc70a7660e4645eedcc5fb85816f2e3e24dc"'
})
